const request = require('supertest');

const app = require('../src/app');
const { sequelize } = require('../src/api/models');
const { queryInterface } = sequelize;

beforeAll((done) => {
  queryInterface.bulkInsert('Tasks', [
    {
      id: 1,
      title: 'Belajar Membuat Aplikasi Backend Untuk Pemula',
      status: false,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      id: 2,
      title: 'Menjadi Fullstack Developer',
      status: false,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      id: 3,
      title: 'Baca Buku React',
      status: true,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      id: 4,
      title: 'Baca Buku Node.js',
      status: false,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      id: 5,
      title: 'Mencoba Bahasa Pemrograman Golang',
      status: false,
      createdAt: new Date(),
      updatedAt: new Date()
    },
  ], {})
  .then(_ => {
      done()
  })
  .catch((err) => {
    console.log(err);
    done(err);
  })
})

afterAll((done) => {
  queryInterface.bulkDelete('Tasks', null, {})
    .then(_ => {
        done();
    })
    .catch(err => {
        console.log(err);
        done(err);
    })
});

describe('Test task route', () => {
  it('GET /tasks should return all tasks', (done) => {
    request(app)
      .get('/api/tasks')
      .expect('Content-Type', /json/)
      .then((res) => {
        const { statusCode, body } = res;

        expect(statusCode).toEqual(200);

        const tasks = body.data.tasks;

        expect(tasks).toEqual(expect.any(Array));
        expect(tasks.length).toEqual(5);
        
        const firstData = tasks[0];
        expect(firstData.title).toEqual('Belajar Membuat Aplikasi Backend Untuk Pemula');
        expect(firstData.status).toEqual(false);

        done();
      })
      .catch((err) => {
        console.log(err);
        done(err);
      });
  });

  it('GET /tasks/:id should return task with id 1', (done) => {
    request(app)
      .get('/api/tasks/1')
      .expect('Content-Type', /json/)
      .then((res) => {
        const { statusCode, body } = res;

        expect(statusCode).toEqual(200);

        const task = body.data.task;

        expect(task.title).toEqual('Belajar Membuat Aplikasi Backend Untuk Pemula');
        expect(task.status).toEqual(false);

        done();
      })
      .catch((err) => {
        console.log(err);
        done(err);
      });
  });

  it('GET /tasks/:id should return 404 if task not found', (done) => {
    request(app)
      .get('/api/tasks/999')
      .expect('Content-Type', /json/)
      .then((res) => {
        const { statusCode, body } = res;

        expect(statusCode).toEqual(404);
        expect(body.success).toBeFalsy();

        done();
      })
      .catch((err) => {
        console.log(err);
        done(err);
      });
  });

  it('POST /tasks should create new task', (done) => {
    request(app)
      .post('/api/tasks')
      .send({
        id: 6,
        title: 'Membuat Aplikasi Backend Untuk Pemula',
        status: false
      })
      .expect('Content-Type', /json/)
      .then((res) => {
        const { statusCode, body } = res;

        expect(statusCode).toEqual(201);

        const task = body.data.task;

        expect(task.title).toEqual('Membuat Aplikasi Backend Untuk Pemula');
        expect(task.status).toEqual(false);

        done();
      })
      .catch((err) => {
        console.log(err);
        done(err);
      });
  });

  it('POST /tasks should return 400 if title is empty', (done) => {
    request(app)
      .post('/api/tasks')
      .send({
        title: '',
        status: false
      })
      .expect('Content-Type', /json/)
      .then((res) => {
        const { statusCode, body } = res;

        expect(statusCode).toEqual(400);
        expect(body.success).toBeFalsy();

        done();
      })
      .catch((err) => {
        console.log(err);
        done(err);
      });
  });

  it('PUT /tasks/:id should update task with id 1', (done) => {
    request(app)
      .put('/api/tasks/1')
      .send({
        title: 'Belajar Membuat Aplikasi Backend Untuk Pemula',
        status: true
      })
      .expect('Content-Type', /json/)
      .then((res) => {
        const { statusCode, body } = res;

        expect(statusCode).toEqual(200);

        const task = body.data.task;

        expect(task.title).toEqual('Belajar Membuat Aplikasi Backend Untuk Pemula');
        expect(task.status).toEqual(true);

        done();
      })
      .catch((err) => {
        console.log(err);
        done(err);
      });
  });

  it('PUT /tasks/:id should return 404 if task not found', (done) => {
    request(app)
      .put('/api/tasks/999')
      .send({
        title: 'Belajar Membuat Aplikasi Backend Untuk Pemula',
        status: true
      })
      .expect('Content-Type', /json/)
      .then((res) => {
        const { statusCode, body } = res;

        expect(statusCode).toEqual(404);
        expect(body.success).toBeFalsy();

        done();
      })
      .catch((err) => {
        console.log(err);
        done(err);
      });
  });

  it('PUT /tasks/:id should return 400 if title is empty', (done) => {
    request(app)
      .put('/api/tasks/1')
      .send({
        title: '',
        status: true
      })
      .expect('Content-Type', /json/)
      .then((res) => {
        const { statusCode, body } = res;

        expect(statusCode).toEqual(400);
        expect(body.success).toBeFalsy();

        done();
      })
      .catch((err) => {
        console.log(err);
        done(err);
      });
  });

  it('DELETE /tasks/:id should delete task with id 1', (done) => {
    request(app)
      .delete('/api/tasks/1')
      .expect('Content-Type', /json/)
      .then((res) => {
        const { statusCode, body } = res;

        expect(statusCode).toEqual(200);
        expect(body.success).toBeTruthy();

        done();
      })
      .catch((err) => {
        console.log(err);
        done(err);
      });
  });

  it('DELETE /tasks/:id should return 404 if task not found', (done) => {
    request(app)
      .delete('/api/tasks/999')
      .expect('Content-Type', /json/)
      .then((res) => {
        const { statusCode, body } = res;

        expect(statusCode).toEqual(404);
        expect(body.success).toBeFalsy();

        done();
      })
      .catch((err) => {
        console.log(err);
        done(err);
      });
  });
});
