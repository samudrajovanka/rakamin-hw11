FROM node:18-alpine3.18

COPY package*.json ./

WORKDIR /task-api-hw11

COPY . .

RUN npm install

EXPOSE 5000

CMD ["node", "./bin/www.js"]