const { error, notFound } = require('./error');

module.exports = {
  notFound,
  error
};

