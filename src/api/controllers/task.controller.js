const { Task } = require('../models');
const { NotFoundError, InvariantError } = require('../../exceptions');

class TaskController {
  static create = async (req, res, next) => {
    console.log(req);
    try {
      if (!req.body.title) {
        throw new InvariantError('Title is required');
      }

      const { title, status = false } = req.body;

      const task = await Task.create({
        title,
        status
      });

      return res.status(201).json({
        success: true,
        message: 'Task created successfully',
        data: {
          task
        }
      });
    } catch (error) {
      next(error);
    }
  }
  
  static findAll = async (req, res, next) => {
    try {
      const tasks = await Task.findAll();

      return res.status(200).json({
        success: true,
        message: 'Tasks retrieved successfully',
        data: {
          tasks
        }
      });
    } catch (error) {
      next(error)
    }
  }

  static findOne = async (req, res, next) => {
    try {
      const { id } = req.params;

      const task = await Task.findByPk(id);

      if (!task) {
        throw new NotFoundError();
      }

      return res.status(200).json({
        success: true,
        message: 'Task retrieved successfully',
        data: {
          task
        }
      });
    } catch (error) {
      next(error);
    }
  }

  static updateById = async (req, res, next) => {
    try {
      // validation
      if (!req.body.title) {
        throw new InvariantError('Title is required');
      }

      const { title, status = false } = req.body;

      // get by id
      const { id } = req.params;

      const task = await Task.findByPk(id);

      if (!task) {
        throw new NotFoundError();
      }

      // update
      task.title = title;
      task.status = status;

      await task.save();

      return res.status(200).json({
        success: true,
        message: 'Task updated successfully',
        data: {
          task
        }
      });
    } catch (error) {
      next(error);
    }
  }

  static deleteById = async (req, res, next) => {
    try {
      const { id } = req.params;

      const task = await Task.findByPk(id);

      if (!task) {
        throw new NotFoundError();
      }

      await task.destroy();

      return res.status(200).json({
        success: true,
        message: 'Task deleted successfully'
      });
    } catch (error) {
      next(error);
    }
  }

}

module.exports = TaskController;
