const router = require('express').Router();

const TaskController = require('../controllers/task.controller');

router.get('/', TaskController.findAll);
router.get('/:id', TaskController.findOne);
router.post('/', TaskController.create);
router.put('/:id', TaskController.updateById);
router.delete('/:id', TaskController.deleteById);

module.exports = router;
