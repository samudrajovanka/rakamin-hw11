const router = require('express').Router();

const taskRoute = require('./api/routes/task.route');

/**
 * api routes
 */
router.use('/api/tasks', taskRoute);

module.exports = router;
