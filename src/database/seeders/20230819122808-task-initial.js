'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('Tasks', [
      {
        title: 'Belajar Membuat Aplikasi Backend Untuk Pemula',
        status: false,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'Menjadi Fullstack Developer',
        status: false,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'Baca Buku React',
        status: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'Baca Buku Node.js',
        status: false,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'Mencoba Bahasa Pemrograman Golang',
        status: false,
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ]);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    return queryInterface.bulkDelete('Tasks', null, {});
  }
};
